// SPDX-FileCopyrightText: 2024 Matthieu Gallien <matthieu_gallien@yahoo.fr>
//
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

#include "nextcloudauthentication.h"

#include "loginflowv2.h"

class NextcloudAuthenticationPrivate
{
public:
    explicit NextcloudAuthenticationPrivate(QNetworkAccessManager &accessManager)
        : m_loginFlow{accessManager}
    {
    }

    QUrl m_serverUrl;
    QUrl m_initialServerUrl;
    QString m_loginName;
    QString m_appPassword;
    LoginFlowV2 m_loginFlow;
};

NextcloudAuthentication::NextcloudAuthentication(QNetworkAccessManager &accessManager,
                                                 QObject *parent)
    : QObject{parent}
    , d{std::make_unique<NextcloudAuthenticationPrivate>(accessManager)}
{
    connect(&d->m_loginFlow, &LoginFlowV2::fail,
            this, &NextcloudAuthentication::loginFailed);
    connect(&d->m_loginFlow, &LoginFlowV2::done,
            this, [this] (QUrl serverUrl, QString loginName, QString appPassword) {
        d->m_serverUrl = serverUrl;
        Q_EMIT serverUrlChanged();
        d->m_loginName = loginName;
        Q_EMIT loginNameChanged();
        d->m_appPassword = appPassword;
        Q_EMIT appPasswordChanged();
        Q_EMIT loginSucceeded(serverUrl, loginName, appPassword);
    });
}

NextcloudAuthentication::~NextcloudAuthentication() = default;

QUrl NextcloudAuthentication::initialServerUrl() const
{
    return d->m_initialServerUrl;
}

QUrl NextcloudAuthentication::serverUrl() const
{
    return d->m_serverUrl;
}

QString NextcloudAuthentication::loginName() const
{
    return d->m_loginName;
}

QString NextcloudAuthentication::appPassword() const
{
    return d->m_appPassword;
}

void NextcloudAuthentication::setInitialServerUrl(const QUrl &newInitialServerUrl)
{
    if (d->m_initialServerUrl == newInitialServerUrl) {
        return;
    }

    d->m_initialServerUrl = newInitialServerUrl;
    Q_EMIT initialServerUrlChanged();
}

void NextcloudAuthentication::requestAppPassword()
{
    d->m_loginFlow.start(d->m_initialServerUrl);
}
