// SPDX-FileCopyrightText: 2024 Matthieu Gallien <matthieu_gallien@yahoo.fr>
//
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

#ifndef LOGINFLOWV2_H
#define LOGINFLOWV2_H

#include "nextcloudlib_export.h"

#include <QObject>
#include <QUrl>
#include <QString>

#include <optional>
#include <tuple>

class QNetworkAccessManager;
class QJsonDocument;

class NEXTCLOUDLIB_NO_EXPORT LoginFlowV2 : public QObject
{
    Q_OBJECT
public:
    explicit LoginFlowV2(QNetworkAccessManager &accessManager,
                         QObject *parent = nullptr);

Q_SIGNALS:
    void done(QUrl serverUrl,
              QString loginName,
              QString appPassword);

    void fail();

public Q_SLOTS:
    void start(const QUrl &serverLoginUrl);

private:

    void initFlow(const QUrl &serverLoginUrl);

    bool startWebBrowser(const QUrl &loginUrl);

    void startPollingAppToken(QUrl pollUrl,
                              QString token);

    static std::optional<std::tuple<QUrl, QUrl, QString>> parseLoginReply(const QJsonDocument &reply);

    static std::optional<std::tuple<QUrl, QString, QString>> parseTokenReply(const QJsonDocument &reply);

    QNetworkAccessManager &m_accessManager;
};

#endif // LOGINFLOWV2_H
