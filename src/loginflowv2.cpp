// SPDX-FileCopyrightText: 2024 Matthieu Gallien <matthieu_gallien@yahoo.fr>
//
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

#include "loginflowv2.h"

#include "nextcloudlog.h"

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDesktopServices>
#include <QTimer>

constexpr auto pollingTimeout = 10000;

LoginFlowV2::LoginFlowV2(QNetworkAccessManager &accessManager,
                         QObject *parent)
    : QObject{parent}
    , m_accessManager{accessManager}
{
}

void LoginFlowV2::start(const QUrl &serverLoginUrl)
{
    initFlow(serverLoginUrl);
}

void LoginFlowV2::initFlow(const QUrl &serverLoginUrl)
{
    auto loginRequest = QNetworkRequest{serverLoginUrl};
    auto loginReply = m_accessManager.post(loginRequest, QByteArray{});
    connect(loginReply, &QNetworkReply::errorOccurred,
            this, [this] (auto e) {
                qCDebug(nextcloud) << "errorOccurred" << e;
                Q_EMIT fail();
            });
    connect(loginReply, &QNetworkReply::finished,
            this, [loginReply, this] () {
                qCDebug(nextcloud) << "finished";
                const auto replyContent = loginReply->readAll();
                const auto jsonReplyContent = QJsonDocument::fromJson(replyContent);
                const auto result = parseLoginReply(jsonReplyContent);
                if (result) {
                    if (startWebBrowser(std::get<0>(*result))) {
                        startPollingAppToken(std::get<1>(*result), std::get<2>(*result));
                    }
                } else {
                    Q_EMIT fail();
                }
            });
}

bool LoginFlowV2::startWebBrowser(const QUrl &loginUrl)
{
    if (!QDesktopServices::openUrl(loginUrl)) {
        qCDebug(nextcloud) << "failed to open browser for" << loginUrl;
        Q_EMIT fail();
        return false;
    }
    return true;
}

void LoginFlowV2::startPollingAppToken(QUrl pollUrl,
                                       QString token)
{
    auto pollingTimer = new QTimer{};
    pollingTimer->setSingleShot(false);
    pollingTimer->setInterval(pollingTimeout);
    connect(pollingTimer, &QTimer::timeout, pollingTimer, [this,
                                                           pollingTimer,
                                                           pollUrl = std::move(pollUrl),
                                                           token = std::move(token)] () {
        qCDebug(nextcloud) << "start one get request" << pollingTimer;
        auto pollRequestData = QByteArray{"token="} + token.toLatin1();
        auto tokenRequest = QNetworkRequest{pollUrl};
        tokenRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        qCDebug(nextcloud) << "POST" << pollUrl << pollRequestData;
        auto tokenReply = m_accessManager.post(tokenRequest, pollRequestData);
        connect(tokenReply, &QNetworkReply::errorOccurred,
                this, [] (auto e) {
                    qCDebug(nextcloud) << "no token" << e;
                });
        connect(tokenReply, &QNetworkReply::finished,
                this, [pollingTimer, tokenReply, this] () {
                    switch(tokenReply->error()) {
                    case QNetworkReply::NoError:
                    {
                        qCDebug(nextcloud) << "app token found";
                        const auto tokenReplyContent = tokenReply->readAll();
                        const auto jsonTokenReplyContent = QJsonDocument::fromJson(tokenReplyContent);
                        const auto tokenResult = parseTokenReply(jsonTokenReplyContent);
                        if (tokenResult) {
                            Q_EMIT done(std::get<0>(*tokenResult), std::get<1>(*tokenResult), std::get<2>(*tokenResult));
                        } else {
                            Q_EMIT fail();
                        }
                        pollingTimer->stop();
                        pollingTimer->deleteLater();
                    }
                    case QNetworkReply::ContentOperationNotPermittedError:
                    case QNetworkReply::ContentNotFoundError:
                    case QNetworkReply::InternalServerError:
                        break;
                    default:
                        pollingTimer->stop();
                        pollingTimer->deleteLater();
                        Q_EMIT fail();
                    }
                });
    });
    pollingTimer->start();
}

std::optional<std::tuple<QUrl, QUrl, QString>> LoginFlowV2::parseLoginReply(const QJsonDocument &reply)
{
    std::optional<std::tuple<QUrl, QUrl, QString>> result;

    auto loginUrl = QUrl{};
    auto endpointUrl = QUrl{};
    auto token = QString{};

    const auto replyObject = reply.object();
    auto loginData = replyObject.find("login");
    if (loginData != replyObject.end()) {
        loginUrl = QUrl::fromUserInput(loginData->toString());
        qCInfo(nextcloud) << "loginUrl" << loginUrl;
    } else {
        return result;
    }
    auto pollData = replyObject.find("poll");
    if (pollData != replyObject.end()) {
        const auto pollObject = pollData->toObject();
        auto endpointData = pollObject.find("endpoint");
        if (endpointData != pollObject.end()) {
            endpointUrl = QUrl::fromUserInput(endpointData->toString());
            qCInfo(nextcloud) << "endpointUrl" << endpointUrl;
        } else {
            return result;
        }
        auto tokenData = pollObject.find("token");
        if (tokenData != pollObject.end()) {
            token = tokenData->toString();
            qCInfo(nextcloud) << "token" << token;
        } else {
            return result;
        }
    } else {
        return result;
    }

    result = std::make_tuple(loginUrl, endpointUrl, token);
    return result;
}

std::optional<std::tuple<QUrl, QString, QString>> LoginFlowV2::parseTokenReply(const QJsonDocument &reply)
{
    std::optional<std::tuple<QUrl, QString, QString>> result;

    auto serverUrl = QUrl{};
    auto loginName = QString{};
    auto appToken = QString{};

    const auto replyObject = reply.object();
    auto loginData = replyObject.find("server");
    if (loginData != replyObject.end()) {
        serverUrl = QUrl::fromUserInput(loginData->toString());
        qCInfo(nextcloud) << "loginUrl" << serverUrl;
    } else {
        return result;
    }
    auto endpointData = replyObject.find("loginName");
    if (endpointData != replyObject.end()) {
        loginName = endpointData->toString();
        qCInfo(nextcloud) << "endpointUrl" << loginName;
    } else {
        return result;
    }
    auto tokenData = replyObject.find("appPassword");
    if (tokenData != replyObject.end()) {
        appToken = tokenData->toString();
        qCInfo(nextcloud) << "token" << appToken;
    } else {
        return result;
    }

    result = std::make_tuple(serverUrl, loginName, appToken);
    return result;
}
