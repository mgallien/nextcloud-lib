// SPDX-FileCopyrightText: 2024 Matthieu Gallien <matthieu_gallien@yahoo.fr>
//
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

#ifndef NEXTCLOUDAUTHENTICATION_H
#define NEXTCLOUDAUTHENTICATION_H

#include "nextcloudlib_export.h"

#include <QObject>
#include <QUrl>
#include <QString>

#include <memory>

class QNetworkAccessManager;

class NextcloudAuthenticationPrivate;

class NEXTCLOUDLIB_EXPORT NextcloudAuthentication : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QUrl initialServerUrl READ initialServerUrl WRITE setInitialServerUrl NOTIFY initialServerUrlChanged FINAL)
    Q_PROPERTY(QUrl serverUrl READ serverUrl NOTIFY serverUrlChanged FINAL)
    Q_PROPERTY(QString loginName READ loginName NOTIFY loginNameChanged FINAL)
    Q_PROPERTY(QString appPassword READ appPassword NOTIFY appPasswordChanged FINAL)
public:
    explicit NextcloudAuthentication(QNetworkAccessManager &accessManager,
                                     QObject *parent = nullptr);

    ~NextcloudAuthentication() override;

    [[nodiscard]] QUrl initialServerUrl() const;

    [[nodiscard]] QUrl serverUrl() const;

    [[nodiscard]] QString loginName() const;

    [[nodiscard]] QString appPassword() const;

public Q_SLOTS:
    void setInitialServerUrl(const QUrl &newInitialServerUrl);

    void requestAppPassword();

Q_SIGNALS:
    void serverUrlChanged();

    void initialServerUrlChanged();

    void loginNameChanged();

    void appPasswordChanged();

    void loginFailed();

    void loginSucceeded(QUrl serverUrl, QString loginName, QString appPassword);

private:
    std::unique_ptr<NextcloudAuthenticationPrivate> d;
};

#endif // NEXTCLOUDAUTHENTICATION_H
