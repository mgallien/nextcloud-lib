# SPDX-FileCopyrightText: 2024 Matthieu Gallien <matthieu_gallien@yahoo.fr>
#
# SPDX-License-Identifier: BSD-2-Clause

cmake_minimum_required(VERSION 3.28)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

project(nextcloud-lib
    VERSION 0.0.1
    LANGUAGES CXX)

include(FeatureSummary)

set(QT_MAJOR_VERSION 6)

find_package(ECM REQUIRED NO_MODULE)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH})

include(KDEInstallDirs)
include(KDECompilerSettings)
include(KDECMakeSettings)
include(ECMGenerateExportHeader)
include(ECMSetupVersion)
include(ECMGenerateHeaders)
include(ECMQtDeclareLoggingCategory)

find_package(Qt6 REQUIRED COMPONENTS Core Network Gui)

set(nextcloud-lib_version_header "${CMAKE_CURRENT_BINARY_DIR}/src/nextcloud-lib_version.h")
ecm_setup_version(PROJECT
    VARIABLE_PREFIX NextcloudLib
    VERSION_HEADER "${nextcloud-lib_version_header}"
    PACKAGE_VERSION_FILE "${CMAKE_CURRENT_BINARY_DIR}/NextcloudLibConfigVersion.cmake"
    SOVERSION 6)

add_subdirectory(src)
add_subdirectory(tests)

# create a Config.cmake and a ConfigVersion.cmake file and install them
set(CMAKECONFIG_INSTALL_DIR "${KDE_INSTALL_CMAKEPACKAGEDIR}/NextcloudLib")

include(CMakePackageConfigHelpers)

configure_package_config_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/NextcloudLibConfig.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/NextcloudLibConfig.cmake"
    INSTALL_DESTINATION ${CMAKECONFIG_INSTALL_DIR}
)

install(FILES ${nextcloud-lib_version_header}
        DESTINATION ${KDE_INSTALL_INCLUDEDIR}/NextcloudLib
        COMPONENT Devel)

install(FILES
            "${CMAKE_CURRENT_BINARY_DIR}/NextcloudLibConfig.cmake"
            "${CMAKE_CURRENT_BINARY_DIR}/NextcloudLibConfigVersion.cmake"
        DESTINATION "${CMAKECONFIG_INSTALL_DIR}"
        COMPONENT Devel)

install(EXPORT NextcloudLibTargets
        DESTINATION "${CMAKECONFIG_INSTALL_DIR}"
        FILE NextcloudLibTargets.cmake
        NAMESPACE NextcloudLib::)

include(ECMFeatureSummary)
ecm_feature_summary(WHAT ALL   FATAL_ON_MISSING_REQUIRED_PACKAGES)
