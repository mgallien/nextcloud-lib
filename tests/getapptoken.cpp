// SPDX-FileCopyrightText: 2024 Matthieu Gallien <matthieu_gallien@yahoo.fr>
//
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

#include "nextcloudauthentication.h"

#include <QGuiApplication>
#include <QNetworkAccessManager>
#include <QDebug>
#include <QObject>

int main(int argc, char *argv[])
{
    auto app = QGuiApplication{argc, argv};

    auto networkAccess = QNetworkAccessManager{};

    auto test = NextcloudAuthentication{networkAccess};

    QObject::connect(&test, &NextcloudAuthentication::loginSucceeded,
                     &test, [&app] (QUrl serverUrl, QString loginName, QString appPassword) {
        qDebug() << "login info"
                 << "server URL:" << serverUrl
                 << "login name:" << loginName
                 << "app password:" << appPassword;
        app.quit();
    });
    QObject::connect(&test, &NextcloudAuthentication::loginFailed,
                     &test, [&app] () {
        qDebug() << "login failed";
        app.quit();
    });

    test.setInitialServerUrl(QUrl{"https://nextcloud.local/index.php/login/v2"});
    test.requestAppPassword();

    return app.exec();
}
